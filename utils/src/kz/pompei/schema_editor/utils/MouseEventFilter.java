package kz.pompei.schema_editor.utils;

import java.awt.event.MouseEvent;

public class MouseEventFilter {

  public static boolean mask(MouseEvent e, int onMask, int offMask) {
    return (e.getModifiersEx() & (onMask | offMask)) == onMask;
  }

}
