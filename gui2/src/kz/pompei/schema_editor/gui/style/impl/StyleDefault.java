package kz.pompei.schema_editor.gui.style.impl;

import kz.pompei.schema_editor.gui.style.Style;
import kz.pompei.schema_editor.gui.style.StyleTitleBar;

public class StyleDefault implements Style {
  @Override
  public StyleTitleBar titleBar() {
    return titleBar;
  }

  @SuppressWarnings("Convert2Lambda")
  private final StyleTitleBar titleBar = new StyleTitleBar() {
    @Override
    public int height() {
      return 30;
    }
  };
}
