package kz.pompei.schema_editor.gui.util;

import java.awt.Point;
import java.awt.Rectangle;

public class GeoUtil {

  public static Point locatePointOver(Rectangle rect, Point point) {
    if (point.x < rect.x || point.x > rect.x + rect.width || point.y < rect.y || point.y > rect.y + rect.height) {
      return null;
    }

    return new Point(point.x - rect.x, point.y = rect.y);
  }

}
