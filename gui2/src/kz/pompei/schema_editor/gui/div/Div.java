package kz.pompei.schema_editor.gui.div;

import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;

public abstract class Div {
  public int width, height;

  public abstract void paint(Graphics2D g);

  public abstract Cursor getCursorAt(Point point);
}
