package kz.pompei.schema_editor.gui.title_bar;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import kz.pompei.schema_editor.gui.div.Div;
import kz.pompei.schema_editor.gui.panel.FormBounds;

public class TitleBar extends Div {
  public TitleBar(FormBounds formBounds) {

  }

  @Override
  public void paint(Graphics2D g) {
    g.setColor(new Color(64, 184, 24));
    g.fillRect(0, 0, width, height);
  }

  @Override
  public Cursor getCursorAt(Point point) {
    return Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);
  }
}
