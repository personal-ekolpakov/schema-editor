package kz.pompei.schema_editor.gui.panel;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;

public class FormBoundsImpl implements FormBounds {
  private final Window window;

  public FormBoundsImpl(Window window) {
    this.window = window;
  }


  @Override
  public Point getLocation() {
    return window.getLocation();
  }

  @Override
  public Dimension getSize() {
    return window.getSize();
  }

  @Override
  public void setLocation(int x, int y) {
    window.setLocation(x, y);
  }

  @Override
  public void setSize(int width, int height) {
    window.setSize(width, height);
  }
}
