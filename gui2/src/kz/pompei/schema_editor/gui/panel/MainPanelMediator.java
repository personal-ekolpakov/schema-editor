package kz.pompei.schema_editor.gui.panel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainPanelMediator {

  public static void runMouseEventEngine(MainPanel mainPanel) {
    MouseAdapter mouseAdapter = new MouseAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        mainPanel.mouseMoved(e);
      }
    };
    mainPanel.addMouseMotionListener(mouseAdapter);
    mainPanel.addMouseListener(mouseAdapter);
    mainPanel.addMouseWheelListener(mouseAdapter);
  }

}
