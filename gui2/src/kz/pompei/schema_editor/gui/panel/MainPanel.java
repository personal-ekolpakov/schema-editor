package kz.pompei.schema_editor.gui.panel;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import kz.pompei.schema_editor.gui.style.Style;
import kz.pompei.schema_editor.gui.title_bar.TitleBar;

import static kz.pompei.schema_editor.gui.util.GeoUtil.locatePointOver;

public class MainPanel extends JPanel {

  private final Style      style;
  private final TitleBar titleBar;

  public MainPanel(Style style, FormBounds formBounds) {
    this.style      = style;
    titleBar = new TitleBar(formBounds);
  }



  private Rectangle titleBarBounds() {
    int titleBarWidth  = getWidth();
    int titleBarHeight = style.titleBar().height();
    return new Rectangle(0, 0, titleBarWidth, titleBarHeight);
  }

  public Cursor findCursorAt(Point point) {
    Point titleBarPoint = locatePointOver(titleBarBounds(), point);
    if (titleBarPoint != null) {
      return titleBar.getCursorAt(titleBarPoint);
    }
    return Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
  }

  @Override
  protected void paintComponent(Graphics g) {

    {
      Rectangle  bounds    = titleBarBounds();
      Graphics2D titleBarG = (Graphics2D) g.create(bounds.x, bounds.y, bounds.width, bounds.height);
      try {
        titleBar.width  = bounds.width;
        titleBar.height = bounds.height;

        titleBar.paint(titleBarG);
      } finally {
        titleBarG.dispose();
      }
    }

    g.setColor(new Color(0f, 0f, 0f, 0.1f));
    g.fillRect(10, 10, 100, 100);
  }

  Cursor currentCursor = getCursor();

  public void mouseMoved(MouseEvent e) {
    {
      Cursor cursor = findCursorAt(e.getPoint());
      if (System.identityHashCode(cursor) != System.identityHashCode(currentCursor)) {
        currentCursor = cursor;
        setCursor(cursor);
      }
    }
  }
}
