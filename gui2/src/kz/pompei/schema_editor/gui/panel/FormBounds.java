package kz.pompei.schema_editor.gui.panel;

import java.awt.Dimension;
import java.awt.Point;

public interface FormBounds {

  Point getLocation();

  default void setLocation(Point location) {
    setLocation(location.x, location.y);
  }

  Dimension getSize();

  default void setSize(Dimension size) {
    setSize(size.width, size.height);
  }

  void setLocation(int x, int y);

  void setSize(int width, int height);
}
