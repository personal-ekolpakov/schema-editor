package kz.pompei.schema_editor.gui.launcher;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Paths;
import javax.swing.JFrame;
import kz.pompei.schema_editor.gui.panel.FormBounds;
import kz.pompei.schema_editor.gui.panel.FormBoundsImpl;
import kz.pompei.schema_editor.gui.panel.MainPanel;
import kz.pompei.schema_editor.gui.panel.MainPanelMediator;
import kz.pompei.schema_editor.gui.style.Style;
import kz.pompei.schema_editor.gui.style.impl.StyleDefault;
import kz.pompei.schema_editor.utils.SizeLocationSaver;

public class MainFormLauncher {
  public static void main(String[] args) {
    new MainFormLauncher().run();
  }

  private void run() {
    JFrame form = new JFrame();

    form.setLocation(100, 100);
    form.setSize(1024, 400);

    form.setUndecorated(true);

    FormBounds formBounds = new FormBoundsImpl(form);

    Style style = new StyleDefault();

    MainPanel mainPanel = new MainPanel(style, formBounds);
    form.setContentPane(mainPanel);

    SizeLocationSaver.into(Paths.get("build").resolve("sizes_and_locations"))
                     .overJFrame(getClass().getSimpleName(), form);

    form.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        form.dispose();
      }

      @Override
      public void windowClosed(WindowEvent e) {
        System.out.println("YmCyBfe6jP :: Form closed");
      }
    });

    MainPanelMediator.runMouseEventEngine(mainPanel);

    form.setVisible(true);
  }

}
