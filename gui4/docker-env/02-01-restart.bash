#!/usr/bin/env bash

set -e
cd "$(dirname "$0")" || exit 131

docker-compose down
if ! docker-compose up -d
then
  echo "%%%"
  echo "%%% ERROR of : docker-compose up -d"
  echo "%%%"
  exit 17
fi

echo "%%%"
echo "%%% ГОТОВО"
echo "%%%"
