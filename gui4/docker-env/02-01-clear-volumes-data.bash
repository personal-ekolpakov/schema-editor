#!/usr/bin/env bash

set -e
cd "$(dirname "$0")" || exit 131

docker run --rm -v "$HOME/volumes/schema-editor/:/data" \
       busybox:1.35 \
       find /data -mindepth 1 -maxdepth 1 -exec \
       rm -rf {} \;
