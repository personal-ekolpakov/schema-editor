#!/usr/bin/env bash

set -e
cd "$(dirname "$0")" || exit 131

docker-compose down
