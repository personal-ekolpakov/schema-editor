#!/usr/bin/env bash

set -e
cd "$(dirname "$0")" || exit 131

docker-compose down
bash 02-01-clear-volumes-data.bash
