package kz.pompei.schema_editor.gui4.main_form;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SideBarPanel extends JPanel {

  private final Color color;

  @Override
  public void paint(Graphics g) {
    g.setColor(color);
    g.fillRect(0, 0, getWidth(), getHeight());
  }
}
