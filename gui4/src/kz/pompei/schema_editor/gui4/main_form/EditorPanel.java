package kz.pompei.schema_editor.gui4.main_form;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class EditorPanel extends JPanel {

  @Override
  public void paint(Graphics g) {
    g.setColor(new Color(69, 184, 32));
    g.fillRect(0, 0, getWidth(), getHeight());
  }
}
