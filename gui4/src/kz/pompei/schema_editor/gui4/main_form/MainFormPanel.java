package kz.pompei.schema_editor.gui4.main_form;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.SpringLayout;

public class MainFormPanel {

  private static final int sideBarSize   = 25;
  private static final int bottomBarSize = 20;

  private final EditorPanel  editorPanel    = new EditorPanel();
  private final SideBarPanel sideBar_left   = new SideBarPanel(new Color(255, 0, 0));
  private final SideBarPanel sideBar_right  = new SideBarPanel(new Color(18, 24, 213));
  private final SideBarPanel sideBar_bottom = new SideBarPanel(new Color(172, 38, 209));
  private final JFrame       mainForm;

  public MainFormPanel(JFrame mainForm) {
    this.mainForm = mainForm;
    refresh();
  }

  private void refresh() {

    Container contentPane = mainForm.getContentPane();

    while (contentPane.getComponents().length > 0) {
      contentPane.remove(0);
    }

    SpringLayout layout = new SpringLayout();
    contentPane.setLayout(layout);

    {
      contentPane.add(sideBar_left);

      layout.putConstraint(SpringLayout.WEST, sideBar_left, 0,
                           SpringLayout.WEST, contentPane);

      layout.putConstraint(SpringLayout.NORTH, sideBar_left, 0,
                           SpringLayout.NORTH, contentPane);

      layout.putConstraint(SpringLayout.EAST, sideBar_left, sideBarSize,
                           SpringLayout.WEST, contentPane);

      layout.putConstraint(SpringLayout.SOUTH, sideBar_left, 0,
                           SpringLayout.SOUTH, contentPane);
    }
    {
      contentPane.add(sideBar_right);

      layout.putConstraint(SpringLayout.WEST, sideBar_right, -sideBarSize,
                           SpringLayout.EAST, contentPane);

      layout.putConstraint(SpringLayout.NORTH, sideBar_right, 0,
                           SpringLayout.NORTH, contentPane);

      layout.putConstraint(SpringLayout.EAST, sideBar_right, 0,
                           SpringLayout.EAST, contentPane);

      layout.putConstraint(SpringLayout.SOUTH, sideBar_right, 0,
                           SpringLayout.SOUTH, contentPane);


    }
    {
      contentPane.add(sideBar_bottom);

      layout.putConstraint(SpringLayout.WEST, sideBar_bottom, 0,
                           SpringLayout.EAST, sideBar_left);

      layout.putConstraint(SpringLayout.NORTH, sideBar_bottom, -bottomBarSize,
                           SpringLayout.SOUTH, contentPane);

      layout.putConstraint(SpringLayout.EAST, sideBar_bottom, 0,
                           SpringLayout.WEST, sideBar_right);

      layout.putConstraint(SpringLayout.SOUTH, sideBar_bottom, 0,
                           SpringLayout.SOUTH, contentPane);

    }

    {
      contentPane.add(editorPanel);


      layout.putConstraint(SpringLayout.WEST, editorPanel, 10,
                           SpringLayout.EAST, sideBar_left);

      layout.putConstraint(SpringLayout.NORTH, editorPanel, 10,
                           SpringLayout.NORTH, contentPane);

      layout.putConstraint(SpringLayout.EAST, editorPanel, -10,
                           SpringLayout.WEST, sideBar_right);

      layout.putConstraint(SpringLayout.SOUTH, editorPanel, -10,
                           SpringLayout.NORTH, sideBar_bottom);
    }

    contentPane.validate();
  }
}
