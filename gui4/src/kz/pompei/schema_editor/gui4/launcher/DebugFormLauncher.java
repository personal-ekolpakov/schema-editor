package kz.pompei.schema_editor.gui4.launcher;

import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Paths;
import javax.swing.JFrame;
import kz.pompei.schema_editor.gui4.main_form.MainFormPanel;
import kz.pompei.schema_editor.utils.SizeLocationSaver;

public class DebugFormLauncher {
  public static void main(String[] args) {
    DebugFormLauncher debugFormLauncher = new DebugFormLauncher();
    debugFormLauncher.run();
  }

  private void run() {

    JFrame mainForm = new JFrame();
    mainForm.setSize(1024, 400);
    mainForm.setTitle("Schema editor");

    new MainFormPanel(mainForm);

    {
      GraphicsEnvironment   localGraphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
      GraphicsDevice[]      screenDevices            = localGraphicsEnvironment.getScreenDevices();
      GraphicsDevice        lastScreen               = screenDevices[screenDevices.length - 1];
      GraphicsConfiguration defaultConfiguration     = lastScreen.getDefaultConfiguration();
      Rectangle             bounds                   = defaultConfiguration.getBounds();

      Dimension formSize = mainForm.getSize();

      int centerX = bounds.x + bounds.width / 2;
      int centerY = bounds.y + bounds.height / 2;

      int deltaX = formSize.width / 2;
      int deltaY = formSize.height / 2;


      mainForm.setLocation(centerX - deltaX, centerY - deltaY);
    }

    SizeLocationSaver.into(Paths.get("build").resolve("sizes_and_locations"))
                     .overJFrame(mainForm.getClass().getSimpleName(), mainForm);

    mainForm.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        mainForm.dispose();
      }

      @Override
      public void windowClosed(WindowEvent e) {
        System.out.println("a2K50j1V25 :: Application closed");
      }
    });

    mainForm.setVisible(true);

  }
}
