package kz.greetgo.schema_editor.gui.launcher;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Paths;
import javax.swing.JFrame;
import kz.greetgo.schema_editor.gui.panel.MainPanel;
import kz.greetgo.schema_editor.gui.panel.impl.MainPanelStyleFirst;
import kz.pompei.schema_editor.utils.SizeLocationSaver;

public class GuiLauncher {
  public static void main(String[] args) {
    GuiLauncher guiLauncher = new GuiLauncher();
    guiLauncher.run();
  }

  private void run() {

    JFrame form = new JFrame();

    form.setSize(1024, 400);
    form.setLocation(100, 100);
    form.setTitle("Schema Editor");
    form.setUndecorated(true);

    MainPanel mainPanel = new MainPanel(form, new MainPanelStyleFirst());
    form.setContentPane(mainPanel);

    SizeLocationSaver.into(Paths.get("build").resolve("sizes_and_locations"))
                     .overJFrame(form.getClass().getSimpleName(), form);

    form.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        form.dispose();
      }

      @Override
      public void windowClosed(WindowEvent e) {
        System.out.println("F0DP5Ik76Q :: Application Exited");
      }
    });

    form.setVisible(true);
  }
}
