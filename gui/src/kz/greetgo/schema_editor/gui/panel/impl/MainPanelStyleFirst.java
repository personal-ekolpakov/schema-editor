package kz.greetgo.schema_editor.gui.panel.impl;

import kz.greetgo.schema_editor.gui.panel.MainPanelStyle;

public class MainPanelStyleFirst implements MainPanelStyle {
  @Override
  public int titleBarHeight() {
    return 25;
  }

  @Override
  public int horizontalResizerWidth() {
    return 3;
  }

  @Override
  public int verticalResizerHeight() {
    return 3;
  }

  @Override
  public int cornerResizerSize() {
    return 10;
  }
}
