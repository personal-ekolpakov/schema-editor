package kz.greetgo.schema_editor.gui.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import static java.awt.event.InputEvent.BUTTON1_DOWN_MASK;
import static java.awt.event.InputEvent.CTRL_DOWN_MASK;
import static java.awt.event.InputEvent.SHIFT_DOWN_MASK;
import static kz.pompei.schema_editor.utils.MouseEventFilter.mask;

public class TitleBar extends JPanel {

  public TitleBar(JFrame form) {
    MouseAdapter formMover = new MouseAdapter() {

      Point startPoint;
      Point formLocationStartedAt;


      @Override
      public void mouseReleased(MouseEvent e) {
        startPoint            = null;
        formLocationStartedAt = null;
      }

      @Override
      public void mousePressed(MouseEvent e) {
        if (mask(e, BUTTON1_DOWN_MASK, CTRL_DOWN_MASK | SHIFT_DOWN_MASK)) {
          startPoint            = e.getLocationOnScreen();
          formLocationStartedAt = form.getLocation();
          return;
        }
      }

      @Override
      public void mouseDragged(MouseEvent e) {
        Point current = e.getLocationOnScreen();
        if (startPoint == null || formLocationStartedAt == null) {
          return;
        }

        int deltaX = current.x - startPoint.x;
        int deltaY = current.y - startPoint.y;

        form.setLocation(formLocationStartedAt.x + deltaX, formLocationStartedAt.y + deltaY);
      }

    };
    addMouseMotionListener(formMover);
    addMouseListener(formMover);
  }

  @Override
  public void paint(Graphics g) {
    g.setColor(new Color(78, 191, 19));
    g.fillRect(0, 0, getWidth(), getHeight());
  }


}
