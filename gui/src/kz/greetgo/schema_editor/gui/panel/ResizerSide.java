package kz.greetgo.schema_editor.gui.panel;

public enum ResizerSide {

  //@formatter:off
  LEFT           (  true , false, false, false  ),
  LEFT_TOP       (  true , true , false, false  ),
       TOP       (  false, true , false, false  ),
       TOP_RIGHT (  false, true , true , false  ),
           RIGHT (  false, false, true , false  ),
    BOTTOM_RIGHT (  false, false, true , true   ),
    BOTTOM       (  false, false, false, true   ),
    BOTTOM_LEFT  (  true , false, false, true   ),
  //@formatter:on

  ;

  public final boolean left;
  public final boolean top;
  public final boolean right;
  public final boolean bottom;

  ResizerSide(boolean left, boolean top, boolean right, boolean bottom) {
    this.left   = left;
    this.top    = top;
    this.right  = right;
    this.bottom = bottom;
  }
}
