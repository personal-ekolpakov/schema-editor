package kz.greetgo.schema_editor.gui.panel;

import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainPanel extends JPanel {

  private final TitleBar       titleBar;
  private final MainPanelStyle geo;
  private final ResizerPanel   leftResizer;
  private final ResizerPanel   leftTopResizer;
  private final ResizerPanel   topResizer;
  private final ResizerPanel   topRightResizer;
  private final ResizerPanel   rightResizer;
  private final ResizerPanel   bottomRightResizer1;
  private final ResizerPanel   bottomRightResizer2;
  private final ResizerPanel   bottomResizer;
  private final ResizerPanel   bottomLeftResizer1;
  private final ResizerPanel   bottomLeftResizer2;

  public MainPanel(JFrame form, MainPanelStyle geo) {
    this.geo = geo;
    setLayout(null);
    titleBar = new TitleBar(form);

    leftResizer         = new ResizerPanel(form, ResizerSide.LEFT);
    leftTopResizer      = new ResizerPanel(form, ResizerSide.LEFT_TOP);
    topResizer          = new ResizerPanel(form, ResizerSide.TOP);
    topRightResizer     = new ResizerPanel(form, ResizerSide.TOP_RIGHT);
    rightResizer        = new ResizerPanel(form, ResizerSide.RIGHT);
    bottomRightResizer1 = new ResizerPanel(form, ResizerSide.BOTTOM_RIGHT);
    bottomRightResizer2 = new ResizerPanel(form, ResizerSide.BOTTOM_RIGHT);
    bottomResizer       = new ResizerPanel(form, ResizerSide.BOTTOM);
    bottomLeftResizer1  = new ResizerPanel(form, ResizerSide.BOTTOM_LEFT);
    bottomLeftResizer2  = new ResizerPanel(form, ResizerSide.BOTTOM_LEFT);

    add(titleBar);

    add(leftResizer);
    add(leftTopResizer);
    add(topResizer);
    add(topRightResizer);
    add(rightResizer);

    add(bottomRightResizer1);
    if (geo.cornerResizerSize() > geo.horizontalResizerWidth()) {
      add(bottomRightResizer2);
    }

    add(bottomResizer);
    add(bottomLeftResizer1);

    if (geo.cornerResizerSize() > geo.horizontalResizerWidth()) {
      add(bottomLeftResizer2);
    }


  }

  @Override
  protected void paintComponent(Graphics g) {
    titleBar.setLocation(geo.horizontalResizerWidth(), geo.verticalResizerHeight());
    titleBar.setSize(getWidth() - 2 * geo.horizontalResizerWidth(), geo.titleBarHeight());

    leftResizer.setLocation(0, geo.cornerResizerSize());
    leftResizer.setSize(geo.horizontalResizerWidth(), getHeight() - 2 * geo.cornerResizerSize());

    leftTopResizer.setLocation(0, 0);
    leftTopResizer.setSize(geo.cornerResizerSize(), geo.cornerResizerSize());

    topResizer.setLocation(geo.cornerResizerSize(), 0);
    topResizer.setSize(getWidth() - 2 * geo.cornerResizerSize(), geo.verticalResizerHeight());

    topRightResizer.setLocation(getWidth() - geo.cornerResizerSize(), 0);
    topRightResizer.setSize(geo.cornerResizerSize(), geo.cornerResizerSize());

    rightResizer.setLocation(getWidth() - geo.horizontalResizerWidth(), geo.cornerResizerSize());
    rightResizer.setSize(geo.horizontalResizerWidth(), getHeight() - 2 * geo.cornerResizerSize());

    bottomRightResizer1.setLocation(getWidth() - geo.horizontalResizerWidth(), getHeight() - geo.cornerResizerSize());
    bottomRightResizer1.setSize(geo.horizontalResizerWidth(), geo.cornerResizerSize());

    if (geo.cornerResizerSize() > geo.horizontalResizerWidth()) {
      bottomRightResizer2.setLocation(getWidth() - geo.cornerResizerSize(), getHeight() - geo.verticalResizerHeight());
      bottomRightResizer2.setSize(geo.cornerResizerSize() - geo.horizontalResizerWidth(), geo.verticalResizerHeight());
    }

    bottomResizer.setLocation(geo.cornerResizerSize(), getHeight() - geo.verticalResizerHeight());
    bottomResizer.setSize(getWidth() - 2 * geo.cornerResizerSize(), geo.verticalResizerHeight());

    bottomLeftResizer1.setLocation(0, getHeight() - geo.cornerResizerSize());
    bottomLeftResizer1.setSize(geo.horizontalResizerWidth(), geo.cornerResizerSize());

    if (geo.cornerResizerSize() > geo.horizontalResizerWidth()) {
      bottomLeftResizer2.setLocation(geo.horizontalResizerWidth(), getHeight() - geo.verticalResizerHeight());
      bottomLeftResizer2.setSize(geo.cornerResizerSize() - geo.horizontalResizerWidth(), geo.verticalResizerHeight());
    }


  }
}
