package kz.greetgo.schema_editor.gui.panel;

public interface MainPanelStyle {
  int titleBarHeight();

  int horizontalResizerWidth();

  int verticalResizerHeight();

  int cornerResizerSize();
}
