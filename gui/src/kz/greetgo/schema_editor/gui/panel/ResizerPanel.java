package kz.greetgo.schema_editor.gui.panel;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import lombok.NonNull;

import static java.awt.event.InputEvent.BUTTON1_DOWN_MASK;
import static java.awt.event.InputEvent.CTRL_DOWN_MASK;
import static java.awt.event.InputEvent.SHIFT_DOWN_MASK;
import static kz.pompei.schema_editor.utils.MouseEventFilter.mask;

public class ResizerPanel extends JPanel {

  private final ResizerSide side;

  public ResizerPanel(JFrame form, ResizerSide side) {
    this.side = side;
    setCursor(extractCursor(side));

    MouseAdapter formMover = new MouseAdapter() {

      Point startMouse;
      Point oldPos;
      Dimension oldSize;


      @Override
      public void mouseReleased(MouseEvent e) {
        startMouse = null;
        oldPos     = null;
      }

      @Override
      public void mousePressed(MouseEvent e) {
        if (mask(e, BUTTON1_DOWN_MASK, CTRL_DOWN_MASK | SHIFT_DOWN_MASK)) {
          startMouse = e.getLocationOnScreen();
          oldPos     = form.getLocation();
          oldSize    = form.getSize();
          return;
        }
      }

      @Override
      public void mouseDragged(MouseEvent e) {
        Point current = e.getLocationOnScreen();
        if (startMouse == null || oldPos == null || oldSize == null) {
          return;
        }

        int deltaX = current.x - startMouse.x;
        int deltaY = current.y - startMouse.y;

        int newX      = oldPos.x;
        int newY      = oldPos.y;
        int newWidth  = oldSize.width;
        int newHeight = oldSize.height;

        if (side.left) {
          newX += deltaX;
          newWidth -= deltaX;
        } else if (side.right) {
          newWidth += deltaX;
        }

        if (side.top) {
          newY += deltaY;
          newHeight -= deltaY;
        } else if (side.bottom) {
          newHeight += deltaY;
        }

        form.setLocation(newX, newY);
        form.setSize(newWidth, newHeight);
      }

    };
    addMouseMotionListener(formMover);
    addMouseListener(formMover);
  }

  private static @NonNull Cursor extractCursor(ResizerSide side) {
    return Cursor.getPredefinedCursor(switch (side) {
      case LEFT -> Cursor.W_RESIZE_CURSOR;
      case LEFT_TOP -> Cursor.NW_RESIZE_CURSOR;
      case TOP -> Cursor.N_RESIZE_CURSOR;
      case TOP_RIGHT -> Cursor.NE_RESIZE_CURSOR;
      case RIGHT -> Cursor.E_RESIZE_CURSOR;
      case BOTTOM_RIGHT -> Cursor.SE_RESIZE_CURSOR;
      case BOTTOM -> Cursor.S_RESIZE_CURSOR;
      case BOTTOM_LEFT -> Cursor.SW_RESIZE_CURSOR;
    });

  }

  @Override
  public void paint(Graphics g) {

    switch (side) {
      case LEFT, RIGHT, TOP, BOTTOM -> g.setColor(new Color(22, 83, 215));
      default -> g.setColor(new Color(224, 8, 56));
    }

    g.fillRect(0, 0, getWidth(), getHeight());
  }
}
