package kz.pompei.schema_editor.gui3.engine;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.schema_editor.gui3.div.Div;
import kz.pompei.schema_editor.gui3.div.DivHolder;
import kz.pompei.schema_editor.gui3.div.Divan;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DivSynchronizer__001_Test {

  public static class TestDiv extends Div {

    @Override
    public List<Divan> render() {
      return List.of(
        Divan.of(TestSubDiv.class).classes("main")
             .children(
               Divan.of(Div.class).classes("stone stone_selected"),
               Divan.of(Div.class).classes("stone")
             )
      );
    }
  }

  public static class TestSubDiv extends Div {

    @Override
    public List<Divan> render() {
      return List.of(
        Divan.of(Div.class).classes("asd")
             .children(
               Divan.of(Div.class).classes("asd asd1"),
               Divan.of(Div.class).classes("asd asd2")
             )

        ,

        Divan.of(Div.class).classes("asd asd_top")
             .children(
               Divan.of(Div.class).classes("asd1")
                    .children(definedChildren()),
               Divan.of(Div.class).classes("asd2")
             )
      );
    }
  }

  @Test
  public void synchronize() {

    List<Divan>     divans  = new ArrayList<>();
    List<DivHolder> workers = new ArrayList<>();

    Divan.of(TestDiv.class).appendTo(divans);

    DivSynchronizer divSynchronizer = new DivSynchronizer();

    //
    //
    divSynchronizer.synchronize(divans, workers);
    //
    //

    for (final DivHolder worker : workers) {
      worker.print(0);
    }

    assertThat(workers).hasSize(1);
    assertThat(workers.get(0).div).isNotNull().isInstanceOf(TestDiv.class);
    DivHolder testDivHolder = workers.get(0);
    assertThat(testDivHolder.children).hasSize(1);
  }
}
