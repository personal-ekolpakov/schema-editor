package kz.pompei.schema_editor.gui3.styles.table;

import java.util.List;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StyleRequestTest {

  @Test
  public void parseFilter__applyType_01() {

    //
    //
    StyleRequest styleRequest = StyleRequest.parseSelector("asd > dsa qwe ~ tre + wer");
    //
    //

    assertThat(styleRequest.selectors).hasSize(5);
    assertThat(styleRequest.selectors.get(1).applyType).isEqualTo(DivSelectorApplyType.CHILD);
    assertThat(styleRequest.selectors.get(2).applyType).isEqualTo(DivSelectorApplyType.INNER);
    assertThat(styleRequest.selectors.get(3).applyType).isEqualTo(DivSelectorApplyType.AFTER);
    assertThat(styleRequest.selectors.get(4).applyType).isEqualTo(DivSelectorApplyType.NEXT);

  }

  @Test
  public void parseFilter__applyType_02() {

    //
    //
    StyleRequest styleRequest = StyleRequest.parseSelector(" + > ~ asd > dsa qwe ~ tre + wer");
    //
    //

    assertThat(styleRequest.selectors).hasSize(5);
    assertThat(styleRequest.selectors.get(1).applyType).isEqualTo(DivSelectorApplyType.CHILD);
    assertThat(styleRequest.selectors.get(2).applyType).isEqualTo(DivSelectorApplyType.INNER);
    assertThat(styleRequest.selectors.get(3).applyType).isEqualTo(DivSelectorApplyType.AFTER);
    assertThat(styleRequest.selectors.get(4).applyType).isEqualTo(DivSelectorApplyType.NEXT);

  }

  @Test
  public void parseFilter__one__001() {

    //
    //
    DivFilter filter = StyleRequest.parseFilter("Stone");
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("JavaName{Stone}");
  }

  @Test
  public void parseFilter__one__002() {

    //
    //
    DivFilter filter = StyleRequest.parseFilter(".status.main.hello");
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("styleClassExists{hello main status}");
  }

  @Test
  public void parseFilter__one__003() {

    //
    //
    DivFilter filter = StyleRequest.parseFilter("[hello=world|stone]");
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[hello=world|stone]");
  }

  @Test
  public void parseFilter__one__004() {

    //
    //
    DivFilter filter = StyleRequest.parseFilter("[hello!=world|stone]");
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[hello!=world|stone]");
  }

  @Test
  public void parseFilter__one__005() {

    //
    //
    DivFilter filter = StyleRequest.parseFilter("[stone!=white|blue][sky=light-blue]");
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[[stone!=white|blue] AND [sky=light-blue]]");
  }

  @Test
  public void parseFilter__one__006() {

    //
    //
    DivFilter filter = StyleRequest.parseFilter(".status[stone!=white|blue][sky=light-blue]");
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[styleClassExists{status} AND [stone!=white|blue] AND [sky=light-blue]]");
  }

  @Test
  public void parseFilter__one__007() {

    //
    //
    DivFilter filter = StyleRequest.parseFilter(".status:first");
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[styleClassExists{status} AND nth{0n+0:FORWARD}]");
  }

  @Test
  public void parseFilter__one__008() {

    //
    //
    DivFilter filter = StyleRequest.parseFilter(".status:last");
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[styleClassExists{status} AND nth{0n+0:BACKWARD}]");
  }

  @Test
  public void parseFilter__one__009() {

    //
    //
    DivFilter filter = StyleRequest.parseFilter(".status:nth(3n+2)");
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[styleClassExists{status} AND nth{3n+2:FORWARD}]");
  }

  @Test
  public void parseFilter__one__010() {

    //
    //
    DivFilter filter = StyleRequest.parseFilter(":nth-back(3n+2)");
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("nth{3n+2:BACKWARD}");
  }

  @Test
  public void parseFilter__one__011() {

    String filterStr = ".blow.spoon[stone!=white|blue][sun][sky=light-blue]:hover(:nth(5n+2)|:nth(5n+4)|:first|:last)";
    System.out.println("14toT33GoS :: filterStr = `" + filterStr + "`");

    //
    //
    DivFilter filter = StyleRequest.parseFilter(filterStr);
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[styleClassExists{blow spoon} AND [stone!=white|blue] AND [sun] AND [sky=light-blue]" +
                                              " AND [hover AND [nth{5n+2:FORWARD} OR nth{5n+4:FORWARD} OR nth{0n+0:FORWARD}" +
                                              " OR nth{0n+0:BACKWARD}]]]");
  }

  @Test
  public void parseAfterFilter__001() {

    String filterStr = ":hover:placeholder:hover(:nth(5n+2)|nth(5n+4)|first|last)((focus|:selection:nth-back(7n+2))|(:placeholder)):hover";

    //
    //
    DivFilter filter = StyleRequest.parseColonFilter(filterStr);
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[hover AND placeholder AND hover AND [nth{5n+2:FORWARD} OR nth{5n+4:FORWARD}" +
                                              " OR nth{0n+0:FORWARD} OR nth{0n+0:BACKWARD}] AND " +
                                              "[[focus OR [selection AND nth{7n+2:BACKWARD}]] AND placeholder] AND hover]");
  }

  @Test
  public void parseAfterFilter__002() {

    String filterStr = ":hover|:placeholder|:selection";

    //
    //
    DivFilter filter = StyleRequest.parseColonFilter(filterStr);
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[hover OR placeholder OR selection]");
  }

  @Test
  public void parseAfterFilter__003() {

    String filterStr = ":hover:placeholder|:selection";

    //
    //
    DivFilter filter = StyleRequest.parseColonFilter(filterStr);
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString()).isEqualTo("[[hover AND placeholder] OR selection]");
  }

  @Test
  public void parseAfterFilter__004() {

    String filterStr = ":hover(:nth(5n+2)|nth(5n+4)|first|last):hover";

    //
    //
    DivFilter filter = StyleRequest.parseColonFilter(filterStr);
    //
    //

    assertThat(filter).isNotNull();
    assertThat(filter.toString())
      .isEqualTo("[hover AND [nth{5n+2:FORWARD} OR nth{5n+4:FORWARD} OR nth{0n+0:FORWARD} OR nth{0n+0:BACKWARD}] AND hover]");
  }


  @Test
  public void divideSelectorOnSingles__001() {

    String selectorStr = "   asd   +    dsa ~  wow > hello    world   ";

    //
    //
    List<StyleRequest.SingleSelector> lst = StyleRequest.divideSelectorOnSingles(selectorStr);
    //
    //

    for (final StyleRequest.SingleSelector ss : lst) {
      System.out.println("g22mxQ9Vx6 :: ss = " + ss);
    }

    assertThat(lst).hasSize(5);

    assertThat(lst.get(0).applyType).isEqualTo(DivSelectorApplyType.INNER);
    assertThat(lst.get(1).applyType).isEqualTo(DivSelectorApplyType.NEXT);
    assertThat(lst.get(2).applyType).isEqualTo(DivSelectorApplyType.AFTER);
    assertThat(lst.get(3).applyType).isEqualTo(DivSelectorApplyType.CHILD);
    assertThat(lst.get(4).applyType).isEqualTo(DivSelectorApplyType.INNER);

    assertThat(lst.get(0).selectorText).isEqualTo("asd");
    assertThat(lst.get(1).selectorText).isEqualTo("dsa");
    assertThat(lst.get(2).selectorText).isEqualTo("wow");
    assertThat(lst.get(3).selectorText).isEqualTo("hello");
    assertThat(lst.get(4).selectorText).isEqualTo("world");

  }

  @Test
  public void divideSelectorOnSingles__002() {

    String selectorStr = "stone+hello(wow+sin~x)moon~status";

    //
    //
    List<StyleRequest.SingleSelector> lst = StyleRequest.divideSelectorOnSingles(selectorStr);
    //
    //

    for (final StyleRequest.SingleSelector ss : lst) {
      System.out.println("y061NE9Vit :: ss = " + ss);
    }

    assertThat(lst).hasSize(3);

    assertThat(lst.get(0).selectorText).isEqualTo("stone");
    assertThat(lst.get(1).selectorText).isEqualTo("hello(wow+sin~x)moon");
    assertThat(lst.get(2).selectorText).isEqualTo("status");

    assertThat(lst.get(0).applyType).isEqualTo(DivSelectorApplyType.INNER);
    assertThat(lst.get(1).applyType).isEqualTo(DivSelectorApplyType.NEXT);
    assertThat(lst.get(2).applyType).isEqualTo(DivSelectorApplyType.AFTER);


  }

  @Test
  public void divideSelectorOnSingles__003() {

    String selectorStr = ".hello";

    //
    //
    List<StyleRequest.SingleSelector> lst = StyleRequest.divideSelectorOnSingles(selectorStr);
    //
    //

    for (final StyleRequest.SingleSelector ss : lst) {
      System.out.println("GEE4UOlJ8R :: ss = " + ss);
    }

    assertThat(lst).hasSize(1);

    assertThat(lst.get(0).selectorText).isEqualTo(".hello");

    assertThat(lst.get(0).applyType).isEqualTo(DivSelectorApplyType.INNER);

  }

  @Test
  public void divideSelectorOnSingles__004() {

    String selectorStr = ".hello .status";

    //
    //
    List<StyleRequest.SingleSelector> lst = StyleRequest.divideSelectorOnSingles(selectorStr);
    //
    //

    for (final StyleRequest.SingleSelector ss : lst) {
      System.out.println("vjt12AU78K :: ss = " + ss);
    }

    assertThat(lst).hasSize(2);

    assertThat(lst.get(0).selectorText).isEqualTo(".hello");
    assertThat(lst.get(1).selectorText).isEqualTo(".status");

    assertThat(lst.get(0).applyType).isEqualTo(DivSelectorApplyType.INNER);
    assertThat(lst.get(1).applyType).isEqualTo(DivSelectorApplyType.INNER);

  }
}
