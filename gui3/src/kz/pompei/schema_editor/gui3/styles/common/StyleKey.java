package kz.pompei.schema_editor.gui3.styles.common;

import kz.pompei.schema_editor.gui3.div.Div;
import lombok.NonNull;

public interface StyleKey {

  /**
   * Проверяет значение стиля. Если оно не правильное, то возвращает null,
   * а также использует div, чтобы сообщить пользователю, что произошла оплошность
   *
   * @param div   диво, которому принадлежит стиль. Используется для сообщения об ошибочном значении
   * @param value значение указанное пользователем
   * @return правильно значение. Т.е. то же значение, что и пользователя, если оно правильное, или null,
   * если значение указанное пользователем неправильное
   */
  Object validateValue(@NonNull Div div, Object value);
}
