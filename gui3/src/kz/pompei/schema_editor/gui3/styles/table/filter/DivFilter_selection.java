package kz.pompei.schema_editor.gui3.styles.table.filter;

import kz.pompei.schema_editor.gui3.div.DivHolder;
import lombok.NonNull;

public class DivFilter_selection extends DivFilterAbstract {

  @Override
  public boolean inFilter(@NonNull DivHolder divHolder) {
    return false;
  }

  @Override
  public String toString() {
    return filterName();
  }
}
