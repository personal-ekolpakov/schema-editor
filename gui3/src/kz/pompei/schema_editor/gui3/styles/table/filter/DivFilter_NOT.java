package kz.pompei.schema_editor.gui3.styles.table.filter;

import kz.pompei.schema_editor.gui3.div.DivHolder;
import lombok.NonNull;

public class DivFilter_NOT extends DivFilterAbstract {

  private final @NonNull DivFilter operand;

  public DivFilter_NOT(@NonNull DivFilter operand) {
    this.operand = operand;
  }

  @Override
  public boolean inFilter(@NonNull DivHolder divHolder) {
    return !operand.inFilter(divHolder);
  }

  @Override
  public String toString() {
    return "NOT(" + operand + ")";
  }
}
