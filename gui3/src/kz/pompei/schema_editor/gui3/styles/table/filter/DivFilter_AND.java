package kz.pompei.schema_editor.gui3.styles.table.filter;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import kz.pompei.schema_editor.gui3.div.DivHolder;
import lombok.NonNull;

public class DivFilter_AND extends DivFilterAbstract {

  private final List<DivFilter> operands;

  public DivFilter_AND(List<DivFilter> operands) {
    operands.forEach(Objects::requireNonNull);
    this.operands = operands;
  }

  @Override
  public boolean inFilter(@NonNull DivHolder divHolder) {
    for (final DivFilter operand : operands) {
      if (!operand.inFilter(divHolder)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString() {
    return operands.stream().map(Object::toString).collect(Collectors.joining(" AND ", "[", "]"));
  }
}
