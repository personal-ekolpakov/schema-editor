package kz.pompei.schema_editor.gui3.styles.table;

import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DivSelector {
  public final DivSelectorApplyType applyType;
  public final DivFilter            filter;

  @Override
  public String toString() {
    return getClass().getSimpleName() + '{' + applyType + ", " + filter + '}';
  }
}
