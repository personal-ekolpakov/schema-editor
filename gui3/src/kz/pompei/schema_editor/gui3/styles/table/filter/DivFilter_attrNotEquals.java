package kz.pompei.schema_editor.gui3.styles.table.filter;

import java.util.List;
import java.util.Objects;
import kz.pompei.schema_editor.gui3.div.DivHolder;
import lombok.NonNull;

import static java.util.stream.Collectors.joining;

public class DivFilter_attrNotEquals extends DivFilterAbstract {

  private final String       attrKey;
  private final List<String> attrValues;

  public DivFilter_attrNotEquals(String attrKey, List<String> attrValues) {
    this.attrKey    = attrKey;
    this.attrValues = attrValues;
  }

  @Override
  public boolean inFilter(@NonNull DivHolder divHolder) {
    String attrValue = divHolder.getStrAttrMap().get(attrKey);
    for (final String value : attrValues) {
      if (Objects.equals(value, attrValue)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString() {
    return "[" + attrKey + "!=" + attrValues.stream().map(Object::toString).collect(joining("|")) + "]";
  }
}
