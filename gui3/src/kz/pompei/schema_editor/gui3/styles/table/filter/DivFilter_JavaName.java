package kz.pompei.schema_editor.gui3.styles.table.filter;

import kz.pompei.schema_editor.gui3.div.DivHolder;
import lombok.NonNull;

public class DivFilter_JavaName extends DivFilterAbstract {

  private final @NonNull String javaClassSimpleName;

  public DivFilter_JavaName(@NonNull String javaClassSimpleName) {
    this.javaClassSimpleName = javaClassSimpleName;
  }

  @Override
  public boolean inFilter(@NonNull DivHolder divHolder) {
    return javaClassSimpleName.equals(divHolder.div.getClass().getSimpleName());
  }

  @Override
  public String toString() {
    return filterName() + "{" + javaClassSimpleName + "}";
  }
}
