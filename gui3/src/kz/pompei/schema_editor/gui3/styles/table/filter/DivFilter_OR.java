package kz.pompei.schema_editor.gui3.styles.table.filter;

import java.util.List;
import java.util.Objects;
import kz.pompei.schema_editor.gui3.div.DivHolder;
import lombok.NonNull;

import static java.util.stream.Collectors.joining;

public class DivFilter_OR extends DivFilterAbstract {

  private final List<DivFilter> operands;


  public DivFilter_OR(List<DivFilter> operands) {
    operands.forEach(Objects::requireNonNull);
    this.operands = operands;
  }

  @Override
  public boolean inFilter(@NonNull DivHolder divHolder) {
    for (final DivFilter operand : operands) {
      if (operand.inFilter(divHolder)) {
        return true;
      }
    }
    return false;
  }


  @Override
  public String toString() {
    return operands.stream().map(Object::toString).collect(joining(" OR ", "[", "]"));
  }
}
