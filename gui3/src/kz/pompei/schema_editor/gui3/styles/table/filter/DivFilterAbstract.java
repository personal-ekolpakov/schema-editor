package kz.pompei.schema_editor.gui3.styles.table.filter;

public abstract class DivFilterAbstract implements DivFilter {
  protected String filterName() {
    String name = getClass().getSimpleName();
    if (!name.startsWith(DivFilter.class.getSimpleName())) {
      return name;
    }
    {
      String cutName = name.substring(DivFilter.class.getSimpleName().length());
      while (cutName.startsWith("_")) {
        cutName = cutName.substring(1);
      }
      return cutName;
    }
  }
}
