package kz.pompei.schema_editor.gui3.styles.table.filter;

import kz.pompei.schema_editor.gui3.div.DivHolder;
import lombok.NonNull;

public interface DivFilter {
  boolean inFilter(@NonNull DivHolder divHolder);
}
