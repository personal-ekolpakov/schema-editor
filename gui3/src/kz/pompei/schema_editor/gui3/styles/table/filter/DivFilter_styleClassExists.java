package kz.pompei.schema_editor.gui3.styles.table.filter;

import java.util.HashSet;
import java.util.List;
import kz.pompei.schema_editor.gui3.div.DivHolder;
import kz.pompei.schema_editor.gui3.div.StyleClass;
import lombok.NonNull;

import static java.util.stream.Collectors.joining;

public class DivFilter_styleClassExists extends DivFilterAbstract {

  private final @NonNull List<StyleClass> styleClassList;

  public DivFilter_styleClassExists(@NonNull List<StyleClass> styleClassList) {
    this.styleClassList = styleClassList;
  }

  @Override
  public boolean inFilter(@NonNull DivHolder divHolder) {

    HashSet<StyleClass> existsStyleClasses = new HashSet<>(divHolder.getStyleClasses());

    for (final StyleClass styleClass : styleClassList) {
      if (!existsStyleClasses.contains(styleClass)) {
        return false;
      }
    }

    return true;
  }

  @Override
  public String toString() {
    String classes = styleClassList.stream().map(x -> x.className).sorted().collect(joining(" "));
    return filterName() + '{' + classes + '}';
  }
}
