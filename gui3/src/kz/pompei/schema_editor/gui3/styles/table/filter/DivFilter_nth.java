package kz.pompei.schema_editor.gui3.styles.table.filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kz.pompei.schema_editor.gui3.div.DivHolder;
import lombok.NonNull;

public class DivFilter_nth extends DivFilterAbstract {

  /**
   * kn+b
   */
  private final int     k;
  private final int     b;
  private final boolean backward;

  public DivFilter_nth(int k, int b, boolean backward) {
    this.k        = k;
    this.b        = b;
    this.backward = backward;
  }

  @Override
  public boolean inFilter(@NonNull DivHolder divHolder) {
    int i = backward ? divHolder.indexFromEnd : divHolder.indexFromBegin;
    if (k == 0) {
      return i == b;
    }
    if (i < b) {
      return false;
    }
    return (i - b) % k == 0;
  }

  @Override
  public String toString() {
    return filterName() + "{" + k + "n+" + b + ":" + (backward ? "BACKWARD" : "FORWARD") + "}";
  }

  private static final Pattern ARGS = Pattern.compile("(\\d+)n\\+(\\d+)");

  public static DivFilter_nth parse(String name, String txtInBrackets) {
    Matcher matcher = ARGS.matcher(txtInBrackets);
    if (!matcher.matches()) {
      throw new RuntimeException("9W29aSs1r0 :: Непонятный формат аргументов `" + txtInBrackets + "`" +
                                   " для псевдо-элемента `" + name + "`");
    }
    boolean backward = name.endsWith("-back");
    int     k        = Integer.parseInt(matcher.group(1));
    int     b        = Integer.parseInt(matcher.group(2));
    return new DivFilter_nth(k, b, backward);
  }
}
