package kz.pompei.schema_editor.gui3.styles.table;

/**
 * Тип соединения следующего селектора по отношению к предыдущему.
 * <p>
 * Для первого селектора не используется
 */
public enum DivSelectorApplyType {
  /**
   * Выбирает всю иерархию дочерних элементов
   */
  INNER /* пробел */,

  /**
   * Выбирает дочерние элементы (внуков уже не трогает)
   */
  CHILD /* > */,

  /**
   * Выбирает следующий элемент, у которого такой же родитель
   */
  NEXT  /* + */,

  /**
   * Выбирает все следующие элементы, у которых такие же родители
   */
  AFTER /* ~ */,

  ;

  public static DivSelectorApplyType fromChar(char c) {
    return switch (c) {
      case ' ' -> INNER;
      case '>' -> CHILD;
      case '+' -> NEXT;
      case '~' -> AFTER;
      default -> throw new RuntimeException("ODpJh8Qt4N :: Unknown char `" + c + "`");
    };
  }
}
