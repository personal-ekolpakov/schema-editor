package kz.pompei.schema_editor.gui3.styles.table.filter;

import java.util.List;
import java.util.Objects;
import kz.pompei.schema_editor.gui3.div.DivHolder;
import lombok.NonNull;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;

public class DivFilter_attrEquals extends DivFilterAbstract {

  private final String       attrKey;
  private final List<String> attrValues;

  public DivFilter_attrEquals(String attrKey, List<String> attrValues) {
    this.attrKey    = attrKey;
    this.attrValues = attrValues;
  }

  @Override
  public boolean inFilter(@NonNull DivHolder divHolder) {
    String strAttrValue = divHolder.getStrAttrMap().get(attrKey);
    for (final String value : attrValues) {
      if (!Objects.equals(value, strAttrValue)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString() {
    return "[" + attrKey + "=" + String.join("|", attrValues) + "]";
  }
}
