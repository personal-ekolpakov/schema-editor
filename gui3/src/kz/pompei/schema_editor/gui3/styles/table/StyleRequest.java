package kz.pompei.schema_editor.gui3.styles.table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kz.pompei.schema_editor.gui3.div.StyleClass;
import kz.pompei.schema_editor.gui3.styles.common.StyleKey;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_AND;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_JavaName;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_OR;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_attrEquals;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_attrExists;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_attrNotEquals;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_focus;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_hover;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_nth;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_placeholder;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_selection;
import kz.pompei.schema_editor.gui3.styles.table.filter.DivFilter_styleClassExists;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class StyleRequest {

  public final List<DivSelector> selectors = new ArrayList<>();

  public final Map<StyleKey, Object> styles = new HashMap<>();

  @RequiredArgsConstructor
  public static class SingleSelector {
    public final DivSelectorApplyType applyType;
    public final String               selectorText;

    @Override
    public String toString() {
      return "SingleSelector{" + applyType + " # " + selectorText + '}';
    }
  }

  static List<SingleSelector> divideSelectorOnSingles(String selectorStr) {

    String               trimmedSelectorStr = selectorStr.trim();
    List<Integer>        separatorPositions = findSeparatorPositions(trimmedSelectorStr);
    DivSelectorApplyType prevType           = DivSelectorApplyType.INNER;
    List<SingleSelector> ret                = new ArrayList<>();

    int start = 0;
    for (final int pos : separatorPositions) {
      String text = trimmedSelectorStr.substring(start, pos).trim();
      start = pos + 1;
      if (text.length() == 0) {
        continue;
      }

      ret.add(new SingleSelector(prevType, text));

      prevType = DivSelectorApplyType.fromChar(trimmedSelectorStr.charAt(pos));
    }

    if (start < trimmedSelectorStr.length()) {
      String text = trimmedSelectorStr.substring(start).trim();
      ret.add(new SingleSelector(prevType, text));
    }

    return ret;
  }

  private static @NonNull List<Integer> findSeparatorPositions(String trimmedSelectorStr) {
    int           bracketsCount      = 0;
    List<Integer> separatorPositions = new ArrayList<>();

    for (int i = 0, n = trimmedSelectorStr.length(); i < n; i++) {
      char c = trimmedSelectorStr.charAt(i);
      if (c == '(') {
        bracketsCount++;
        continue;
      }
      if (c == ')') {
        bracketsCount--;
        continue;
      }

      if (bracketsCount == 0 && (c == ' ' || c == '+' || c == '~' || c == '>')) {
        separatorPositions.add(i);
      }
    }

    {
      int i = 0;
      while (i < separatorPositions.size() - 1) {
        int pos1 = separatorPositions.get(i);
        int pos2 = separatorPositions.get(i + 1);
        if (pos2 - pos1 > 1) {
          i++;
          continue;
        }
        if (trimmedSelectorStr.charAt(pos1) == ' ') {
          separatorPositions.remove(i);
          continue;
        }
        separatorPositions.remove(i + 1);
      }
    }
    return separatorPositions;
  }

  public static StyleRequest parseSelector(String selectorStr) {
    StyleRequest styleRequest = new StyleRequest();

    List<SingleSelector> singleSelectors = divideSelectorOnSingles(selectorStr);

    for (final SingleSelector singleSelector : singleSelectors) {
      DivFilter            divFilter = parseFilter(singleSelector.selectorText);
      DivSelectorApplyType applyType = singleSelector.applyType;
      styleRequest.selectors.add(new DivSelector(applyType, divFilter));
    }

    return styleRequest;
  }

  public static DivFilter parseFilter(String filterStr) {


    String leftParts  = filterStr;
    String colonParts = null;
    int    colonIndex = filterStr.indexOf(':');
    if (colonIndex >= 0) {
      leftParts  = filterStr.substring(0, colonIndex);
      colonParts = filterStr.substring(colonIndex + 1);
    }


    int idx = 0;

    List<DivFilter> prevFilters   = new ArrayList<>();
    List<DivFilter> squareFilters = new ArrayList<>();

    while (true) {
      int idx1 = leftParts.indexOf('[', idx);
      if (idx1 < 0) {
        break;
      }

      int idx2 = leftParts.indexOf(']', idx1);
      if (idx2 < 0) {
        break;
      }

      String prev   = leftParts.substring(idx, idx1);
      String square = leftParts.substring(idx1 + 1, idx2);
      idx = idx2 + 1;

      if (prev.length() > 0) {
        DivFilter prevFilter = parsePrevFilter(prev);
        if (prevFilter != null) {
          prevFilters.add(prevFilter);
        }
      }

      DivFilter squareFilter = parseSquareFilter(square);
      squareFilters.add(squareFilter);
    }

    if (idx < leftParts.length()) {
      DivFilter prevFilter = parsePrevFilter(leftParts.substring(idx));
      if (prevFilter != null) {
        prevFilters.add(prevFilter);
      }
    }

    List<DivFilter> allFilters = new ArrayList<>(prevFilters);
    allFilters.addAll(squareFilters);

    DivFilter colonFilter = parseColonFilter(colonParts);
    if (colonFilter != null) {
      allFilters.add(colonFilter);
    }

    return filtersAnd(allFilters);

  }

  private static DivFilter filtersAnd(List<DivFilter> filters) {
    if (filters.isEmpty()) {
      return null;
    }
    if (filters.size() == 1) {
      return filters.get(0);
    }
    return new DivFilter_AND(filters);
  }

  private static DivFilter parsePrevFilter(String prev) {

    if (prev.startsWith(".")) {

      List<StyleClass> styleClassList = Arrays.stream(prev.split("\\."))
                                              .filter(s -> s.length() > 0)
                                              .map(StyleClass::new)
                                              .toList();

      if (styleClassList.isEmpty()) {
        return null;
      }

      return new DivFilter_styleClassExists(styleClassList);
    }

    {
      List<String> list = Arrays.stream(prev.split("\\."))
                                .filter(s -> s.length() > 0)
                                .collect(toList());

      if (list.isEmpty()) {
        return null;
      }

      if (list.size() == 1) {
        return new DivFilter_JavaName(list.get(0));
      }

      List<DivFilter> ff = new ArrayList<>();
      ff.add(new DivFilter_JavaName(list.remove(0)));
      ff.add(new DivFilter_styleClassExists(list.stream().map(StyleClass::new).toList()));

      return new DivFilter_AND(ff);
    }
  }

  private static DivFilter parseSquareFilter(String square) {

    boolean eq       = false;
    String  keyStr   = square;
    String  valueStr = null;

    {
      int idx = square.indexOf("!=");
      if (idx >= 0) {
        keyStr   = square.substring(0, idx);
        valueStr = square.substring(idx + 2);
      } else {
        idx = square.indexOf('=');
        if (idx >= 0) {
          eq       = true;
          keyStr   = square.substring(0, idx);
          valueStr = square.substring(idx + 1);
        }
      }
    }

    if (valueStr == null) {
      return new DivFilter_attrExists(keyStr);
    }

    List<String> valueList = Arrays.stream(valueStr.split("\\|")).toList();

    return eq ? new DivFilter_attrEquals(keyStr, valueList) : new DivFilter_attrNotEquals(keyStr, valueList);
  }

  static DivFilter parseColonFilter(String colonParts) {
    if (colonParts == null || colonParts.isEmpty()) {
      return null;
    }

    interface Part {}

    class OR implements Part {
      @Override
      public String toString() {
        return "OR";
      }
    }

    @RequiredArgsConstructor
    class Fil implements Part {
      final DivFilter filter;

      @Override
      public String toString() {
        return "Fil{" + filter + '}';
      }
    }

    @RequiredArgsConstructor
    class Txt implements Part {
      final String txt;

      @Override
      public String toString() {
        return "`" + txt + "`";
      }

      DivFilter parse() {
        return switch (txt.replaceAll(":", "")) {
          case "hover" -> new DivFilter_hover();
          case "focus" -> new DivFilter_focus();
          case "placeholder" -> new DivFilter_placeholder();
          case "selection" -> new DivFilter_selection();
          case "first" -> new DivFilter_nth(0, 0, false);
          case "last" -> new DivFilter_nth(0, 0, true);
          default -> throw new RuntimeException("a3UXxhRu2N :: Unknown pseudo-element `" + txt + "`");
        };
      }
    }

    class Bracket implements Part {
      final List<Part> parts = new ArrayList<>();

      @Override
      public String toString() {
        return "(" + parts.stream().map(Object::toString).collect(joining()) + ')';
      }

      public DivFilter parse() {

        for (int i = 0; i < parts.size(); i++) {
          Part cur = parts.get(i);
          if (cur instanceof Txt txt) {

            if ("nth".equals(txt.txt) || "nth-back".equals(txt.txt)) {

              int i1 = i + 1;
              if (i1 >= parts.size()) {
                throw new RuntimeException("TOXBm4B3O :: No brackets for `" + txt.txt + "`");
              }

              Fil fil = calcFil(txt, i1);
              parts.set(i, fil);
              parts.remove(i1);
              continue;
            }

            if ("|".equals(txt.txt)) {
              parts.set(i, new OR());
              continue;
            }

            Fil fil = new Fil(txt.parse());
            parts.set(i, fil);
            continue;
          }

          if (cur instanceof Bracket b) {
            Fil fil = new Fil(b.parse());
            parts.set(i, fil);
            continue;
          }
        }

        List<List<DivFilter>> division = new ArrayList<>();
        division.add(new ArrayList<>());

        for (final Part part : parts) {

          if (part instanceof Fil fil) {
            division.get(division.size() - 1).add(fil.filter);
            continue;
          }

          if (part instanceof OR) {
            division.add(new ArrayList<>());
            continue;
          }

          throw new RuntimeException("Btp8ERiIIi :: Illegal part " + part);
        }

        List<DivFilter> andList = new ArrayList<>();

        for (final List<DivFilter> filterList : division) {

          if (filterList.isEmpty()) {
            continue;
          }

          if (filterList.size() == 1) {
            andList.add(filterList.get(0));
            continue;
          }

          andList.add(new DivFilter_AND(filterList));
        }

        if (andList.isEmpty()) {
          throw new RuntimeException("66tWpEDPsC :: Unknown filter");
        }

        if (andList.size() == 1) {
          return andList.get(0);
        }

        return new DivFilter_OR(andList);
      }

      private @NonNull Fil calcFil(Txt txt, int i1) {
        if (!(parts.get(i1) instanceof Bracket b)) {
          throw new RuntimeException("QPR8A9L56F :: After `" + txt.txt + "` MUST be bracket");
        }

        if (b.parts.isEmpty()) {
          throw new RuntimeException("c2c0NC313a :: Empty brackets on the right size of `" + txt.txt + "` is wrong");
        }
        if (!(b.parts.get(0) instanceof Txt t)) {
          throw new RuntimeException("l48mgWefQ9 :: Content of brackets is wrong on the right side of `" + txt.txt + "`");
        }

        return new Fil(DivFilter_nth.parse(txt.txt, t.txt));
      }
    }

    @RequiredArgsConstructor
    class Bra {
      final boolean open;
      final int     pos;

      static Bra find(int startIndex, String str) {
        int i1 = str.indexOf('(', startIndex);
        int i2 = str.indexOf(')', startIndex);
        if (i1 < 0 && i2 < 0) {
          return null;
        }

        int     i    = i1 < 0 ? i2 : (i2 < 0 ? i1 : Math.min(i1, i2));
        boolean open = i1 >= 0 && (i2 < 0 || i1 < i2);

        return new Bra(open, i);
      }

      @Override
      public String toString() {
        return "Bra{" + (open ? "open" : "close") + "@" + pos + '}';
      }

      static List<Part> parseTxt(String txt) {
        boolean         startNoOr = !txt.startsWith("|");
        List<String>    list      = Arrays.stream(txt.split("\\|")).filter(s -> s.length() > 0).toList();
        List<List<Txt>> LL        = new ArrayList<>();
        for (final String prt : list) {
          LL.add(Arrays.stream(prt.split(":")).filter(s -> s.length() > 0).map(Txt::new).toList());
        }

        List<Part> ret = new ArrayList<>();
        for (final List<Txt> txtList : LL) {
          if (startNoOr) {
            startNoOr = false;
          } else {
            ret.add(new OR());
          }
          ret.addAll(txtList);
        }

        return ret;
      }
    }

    List<Bracket> brackets = new ArrayList<>();
    brackets.add(new Bracket());

    int pos = 0;

    while (true) {
      Bra bra = Bra.find(pos, colonParts);
      if (bra == null) {
        break;
      }

      String preTxt = colonParts.substring(pos, bra.pos);
      if (preTxt.length() > 0) {
        brackets.get(brackets.size() - 1).parts.addAll(Bra.parseTxt(preTxt));
      }

      pos = bra.pos + 1;

      if (bra.open) {
        Bracket newBracket = new Bracket();
        brackets.get(brackets.size() - 1).parts.add(newBracket);
        brackets.add(newBracket);
        continue;
      }

      {
        brackets.remove(brackets.size() - 1);
      }
    }

    {
      String txt = colonParts.substring(pos);
      if (txt.length() > 0) {
        brackets.get(brackets.size() - 1).parts.addAll(Bra.parseTxt(txt));
      }
    }

    return brackets.get(brackets.size() - 1).parse();
  }
}
