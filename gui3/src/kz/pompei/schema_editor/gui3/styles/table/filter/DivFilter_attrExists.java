package kz.pompei.schema_editor.gui3.styles.table.filter;

import kz.pompei.schema_editor.gui3.div.DivHolder;
import lombok.NonNull;

public class DivFilter_attrExists extends DivFilterAbstract {

  private final String attrKey;

  public DivFilter_attrExists(String attrKey) {
    this.attrKey = attrKey;
  }

  @Override
  public boolean inFilter(@NonNull DivHolder divHolder) {
    return divHolder.getStrAttrMap().containsKey(attrKey);
  }

  @Override
  public String toString() {
    return "[" + attrKey + "]";
  }
}
