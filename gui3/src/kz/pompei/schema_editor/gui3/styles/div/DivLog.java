package kz.pompei.schema_editor.gui3.styles.div;

import kz.pompei.schema_editor.gui3.styles.common.StyleKey;

public interface DivLog {
  void divWrongStyle(String placeId, StyleKey styleKey, Object wrongStyleValue);
}
