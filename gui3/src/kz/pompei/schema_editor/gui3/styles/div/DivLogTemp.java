package kz.pompei.schema_editor.gui3.styles.div;

import kz.pompei.schema_editor.gui3.div.Div;
import kz.pompei.schema_editor.gui3.styles.common.StyleKey;

public class DivLogTemp implements DivLog {
  private final Div logSource;

  public DivLogTemp(Div logSource) {
    this.logSource = logSource;
  }

  @Override
  public void divWrongStyle(String placeId, StyleKey styleKey, Object wrongStyleValue) {
    System.err.println(placeId + " :: Не верный стиль дива " + logSource.getClass().getSimpleName()
                         + " : " + styleKey + " = " + wrongStyleValue);
  }
}
