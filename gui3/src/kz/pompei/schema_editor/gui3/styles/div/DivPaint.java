package kz.pompei.schema_editor.gui3.styles.div;

/**
 * Применяется как значения для стиля {@link Rendering#PAINT}
 */
public enum DivPaint {

  /**
   * Дочерние дивы определяют свои координаты относительно текущего дива
   */
  RELATIVE,

  /**
   * Дочерние дивы определяют свои координаты относительно всей формы
   */
  STATIC,

}
