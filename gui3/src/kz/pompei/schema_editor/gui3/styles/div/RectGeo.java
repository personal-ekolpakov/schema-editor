package kz.pompei.schema_editor.gui3.styles.div;

import java.util.Set;
import kz.pompei.schema_editor.gui3.div.Div;
import kz.pompei.schema_editor.gui3.styles.common.StyleKey;
import lombok.NonNull;

public enum RectGeo implements StyleKey {
  Left, Top, Right, Bottom, Width, Height,
  ;

  private static final Set<Class<?>> VALUE_CLASSES = Set.of(int.class, Integer.class,
                                                            long.class, Long.class,
                                                            double.class, Double.class,
                                                            float.class, Float.class);

  @Override
  public Object validateValue(@NonNull Div div, Object value) {
    Class<?> valueClass = value.getClass();
    if (VALUE_CLASSES.contains(valueClass)) {
      return value;
    }

    div.log().divWrongStyle("wgTQov1nsN", this, value);

    return null;
  }
}
