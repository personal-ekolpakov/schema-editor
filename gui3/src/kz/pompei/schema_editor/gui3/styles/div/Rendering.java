package kz.pompei.schema_editor.gui3.styles.div;

import kz.pompei.schema_editor.gui3.div.Div;
import kz.pompei.schema_editor.gui3.styles.common.StyleKey;
import lombok.NonNull;

public enum Rendering implements StyleKey {

  /**
   * Этот ключ стиля определяет геометрию прорисовки дочерних элементов данного дива.
   * <p>
   * Значение определяются перечислением {@link DivDisplay}
   * <p>
   * Дочерние компоненты не могут определять самостоятельно свои координаты
   */
  DISPLAY {
    @Override
    public Object validateValue(@NonNull Div div, Object value) {
      return validateDisplayValue(div, value);
    }
  },

  /**
   * Этот ключ стиля определяет геометрию прорисовки дочерних элементов так, что дочерние элементы сами определяют свои координаты.
   * <p>
   * Значения определяются перечислением {@link DivPaint}
   */
  PAINT {
    @Override
    public Object validateValue(@NonNull Div div, Object value) {
      return validatePaintValue(div, value);
    }
  },

  /**
   * Этот стиль определяет геометрию прорисовки текущего дива.
   */
  RENDER {
    @Override
    public Object validateValue(@NonNull Div div, Object value) {
      return validateRenderValue(div, value);
    }
  },

  /**
   * Данный стиль определяет, что данный див является формой. Значения логические
   */
  FORM {
    @Override
    public Object validateValue(@NonNull Div div, Object value) {
      return validateFormValue(div, value);
    }
  },

  ;

  private static Object validateDisplayValue(@NonNull Div div, Object value) {
    if (value == null) {
      return null;
    }
    if (value instanceof DivDisplay) {
      return value;
    }
    div.log().divWrongStyle("gk7vL103Qa", DISPLAY, value);
    return null;
  }

  private static Object validatePaintValue(@NonNull Div div, Object value) {
    throw new RuntimeException("sGt2Z9Mc1l");
  }

  private static Object validateRenderValue(@NonNull Div div, Object value) {
    if (value == null) {
      return null;
    }
    if (value instanceof DivRender) {
      return value;
    }
    div.log().divWrongStyle("AS5D4qTNO2", RENDER, value);
    return null;
  }

  private static Object validateFormValue(@NonNull Div div, Object value) {
    if (value == null) {
      return false;
    }
    if (value instanceof Boolean) {
      return value;
    }
    div.log().divWrongStyle("OX37cx1fZt", FORM, value);
    return false;
  }

}
