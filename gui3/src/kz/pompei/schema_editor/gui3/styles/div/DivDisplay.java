package kz.pompei.schema_editor.gui3.styles.div;

/**
 * Применяется как значения для стиля {@link Rendering#DISPLAY}
 */
public enum DivDisplay {
  /**
   * Данный див ни как не прорисовывается, но участвует в логике иерархии дивов.
   * <p>
   * Также если данный стиль не задан, то используется данное значение для дива.
   */
  NONE,

  FLEX,

  GRID,

  FLOW
}
