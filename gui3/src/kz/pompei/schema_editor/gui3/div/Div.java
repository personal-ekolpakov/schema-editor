package kz.pompei.schema_editor.gui3.div;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kz.pompei.schema_editor.gui3.styles.common.StyleKey;
import kz.pompei.schema_editor.gui3.styles.div.DivLog;
import kz.pompei.schema_editor.gui3.styles.div.DivLogTemp;

public class Div {

  private final List<Divan> definedChildren = new ArrayList<>();

  public void init()                                      {}

  public void destroy()                                   {}

  protected void initStyles(Map<StyleKey, Object> styles) {}

  public static class __lib__ {
    public static void appendToDefinedChildren(Div self, List<Divan> divans) {
      self.definedChildren.addAll(divans);
    }
  }

  public List<Divan> definedChildren() {
    return definedChildren;
  }

  public List<Divan> render() {
    return definedChildren();
  }

  private final DivLogTemp log = new DivLogTemp(this);

  public DivLog log() {
    return log;
  }

}
