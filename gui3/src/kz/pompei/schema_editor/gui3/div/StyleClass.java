package kz.pompei.schema_editor.gui3.div;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode
@RequiredArgsConstructor
public final class StyleClass {
  public final String className;

  @Override
  public String toString() {
    return getClass().getSimpleName() + '{' + className + '}';
  }
}
