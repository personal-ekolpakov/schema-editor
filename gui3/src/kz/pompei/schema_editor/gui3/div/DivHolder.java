package kz.pompei.schema_editor.gui3.div;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kz.pompei.schema_editor.gui3.div.attr.AttrKey;
import kz.pompei.schema_editor.gui3.styles.common.StyleKey;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DivHolder {
  public final Divan divan;

  public Div div;

  public int indexFromBegin;

  public int indexFromEnd;

  public boolean mouseHover;

  public final Map<StyleKey, Object> calculatedStyles = new HashMap<>();

  public List<DivHolder> children = new ArrayList<>();

  public List<StyleClass> getStyleClasses() {
    return Divan.__lib__.getStyleClasses(divan);
  }

  public Map<AttrKey, Object> getAttrMap() {
    return Divan.__lib__.getAttrMap(divan);
  }

  public Map<String, String> getStrAttrMap() {
    return getAttrMap().entrySet()
                       .stream()
                       .map(e -> Map.entry(e.getKey().toString(), e.getValue().toString()))
                       .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  public void print(int tab) {
    //noinspection StringBufferReplaceableByString
    StringBuilder sb = new StringBuilder();
    sb.append("  ".repeat(tab));

    sb.append(div.getClass().getSimpleName()).append(" (").append(divan.styleClassesAsStr()).append(')');

    System.out.println(sb);

    for (final DivHolder child : children) {
      child.print(tab + 1);
    }
  }
}
