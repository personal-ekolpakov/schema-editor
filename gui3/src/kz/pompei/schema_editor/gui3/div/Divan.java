package kz.pompei.schema_editor.gui3.div;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kz.pompei.schema_editor.gui3.div.attr.AttrKey;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class Divan {
  public final @NonNull Class<? extends Div> divClass;
  public                String               id;
  private final         List<Divan>          children     = new ArrayList<>();
  private final         List<StyleClass>     styleClasses = new ArrayList<>();
  private final         Map<AttrKey, Object> attrMap      = new HashMap<>();

  public static class __lib__ {
    public static List<Divan> getChildren(Divan self) {
      return self.children;
    }

    public static Map<AttrKey, Object> getAttrMap(Divan self) {
      return self.attrMap;
    }

    public static List<StyleClass> getStyleClasses(Divan self) {
      return self.styleClasses;
    }
  }

  public Divan appendTo(List<Divan> divans) {
    divans.add(this);
    return this;
  }

  public Divan id(String id) {
    this.id = id;
    return this;
  }

  public Divan attr(AttrKey attrKey, Object attrValue) {
    attrMap.put(attrKey, attrValue);
    return this;
  }

  public Divan children(Divan... divans) {
    children.addAll(Arrays.asList(divans));
    return this;
  }

  public Divan children(List<Divan> divans) {
    children.addAll(divans);
    return this;
  }

  public Divan classes(String... classes) {
    for (final String styleClassesStr : classes) {
      for (final String styleClassStr : styleClassesStr.split("\\s+")) {
        styleClasses.add(new StyleClass(styleClassStr));
      }
    }
    return this;
  }

  public String styleClassesAsStr() {
    return styleClasses.stream().map(x -> x.className).collect(Collectors.joining(" "));
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" + divClass.getSimpleName() + " " + styleClassesAsStr() + "}";
  }
}
