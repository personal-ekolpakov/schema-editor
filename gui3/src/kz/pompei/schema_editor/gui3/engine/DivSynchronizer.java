package kz.pompei.schema_editor.gui3.engine;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;
import kz.pompei.schema_editor.gui3.div.Div;
import kz.pompei.schema_editor.gui3.div.DivHolder;
import kz.pompei.schema_editor.gui3.div.Divan;

public class DivSynchronizer {

  private static boolean eqDivan(Divan divan1, Divan divan2) {
    if (divan1 == divan2) {
      return true;
    }
    if (divan1 == null || divan2 == null) {
      return false;
    }
    {
      Class<? extends Div> divClass1 = divan1.divClass;
      Class<? extends Div> divClass2 = divan2.divClass;
      if (divClass1 != divClass2) {
        return false;
      }
    }
    {
      String id1 = divan1.id;
      String id2 = divan2.id;
      return id1 != null && id1.equals(id2);
    }
  }

  public void synchronize(List<Divan> divans, List<DivHolder> workers) {

    int divans_size = divans.size();

    SYNC_FOR:
    for (int i = 0; i < divans_size; i++) {
      Divan divan = divans.get(i);
      if (i >= workers.size()) {
        DivHolder divHolder = createDivanWorker(divan);
        workers.add(divHolder);
        synchronizeDivanWorker(divHolder);
        continue;
      }

      FIND_FOR:
      for (int j = i, n = workers.size(); j < n; j++) {
        DivHolder divHolder = workers.get(j);
        assert divHolder.div != null;
        if (eqDivan(divan, divHolder.divan)) {
          synchronizeDivanWorker(divHolder);
          continue FIND_FOR;
        }

        if (i != j) {
          DivHolder tmp_i = workers.get(i);
          DivHolder tmp_j = workers.get(j);
          workers.set(i, tmp_j);
          workers.set(j, tmp_i);
        }

        synchronizeDivanWorker(workers.get(i));

        continue SYNC_FOR;
      }

      {
        DivHolder divHolder = createDivanWorker(divan);
        workers.add(i, divHolder);
        synchronizeDivanWorker(divHolder);
      }
    }

    while (divans_size < workers.size()) {
      destroyDivanWorkerWithChildren(workers.remove(workers.size() - 1));
    }
  }

  private void synchronizeDivanWorker(DivHolder divHolder) {
    List<Divan>     divans     = divHolder.div.render();
    List<DivHolder> divHolders = divHolder.children;

    synchronize(divans, divHolders);
  }

  private void destroyDivanWorkerWithChildren(DivHolder dyingWorker) {
    LinkedList<DivHolder> toDestroy = new LinkedList<>();
    toDestroy.add(dyingWorker);

    while (!toDestroy.isEmpty()) {
      DivHolder x = toDestroy.pop();
      toDestroy.addAll(x.children);

      destroyDivanWorker(x);
    }
  }

  private void destroyDivanWorker(DivHolder divHolder) {
    divHolder.div.destroy();
  }

  private DivHolder createDivanWorker(Divan divan) {
    DivHolder divHolder = new DivHolder(divan);
    divHolder.div = createDiv(divan);
    Div.__lib__.appendToDefinedChildren(divHolder.div, Divan.__lib__.getChildren(divan));
    divHolder.div.init();
    return divHolder;
  }

  private Div createDiv(Divan divan) {
    Class<? extends Div> divClass = divan.divClass;
    try {
      return divClass.getConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      throw new RuntimeException(e);
    }
  }
}
